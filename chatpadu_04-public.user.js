// ==UserScript==
// @name         chatPadora
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        http://chatpad.jp/chatroom.html
// @require      
// @grant        none
// ==/UserScript==


// v04：ソースコード整理した。どちらがチャット終了したか発言欄の色で表示するようにした。


confirm = function(){
	return true;
}

class ChatPadu{
	constructor(){
		this.setEvent();
		this.buildUI();
		this.setProf();
		
		this.timerChatStartID = null;	
		this.chatstartFlag = false;	// tureの場合、自動巡回停止
		this.ngcheckEnabeld = true;	// falseの場合、NGワードをチェックしない
		this.talkCount = 0;			// 相手の何回目の発言か
		this.talks = [		// 発言ログ
			/* こういう形式のデータが入るが、「入力中」になるバグが多く発生。
			{
				time: '12:34:56',
				person: 'my',
				text: 'こんにちは'
			},
			*/
		];
		this.typingTime = 0;		// 「入力中」になっている秒数のカウント
		this.typingTimeID = null;	// 入力中になるとセットされ、相手の発言があるとクリアされる。
		
	}
	
	// =========================================================================
	// イベント設定
	// =========================================================================
	setEvent(){
		// 新規発言監視
		(()=>{
			const target = document.getElementById('chatLog');
			const observer = new MutationObserver((mutations)=>{
				mutations.forEach(this.eventNewline.bind(this));
			});
			const config = {
				childList: true,	// 子ノードの変更監視
			};
			observer.observe(target, config);
		})();
		
		// プロフィールテキスト変更監視
		(()=>{
			const observer = new MutationObserver((mutations)=>{
				mutations.forEach(this.eventChangeProftext.bind(this));
			});
			observer.observe(
				document.getElementById('reProfileMargin'), {
					childList: true
				}
			);
		})();
		
		// プロフィールが出現した
		(()=>{
			const observer = new MutationObserver((mutations)=>{
				mutations.forEach(this.eventEmergeProf.bind(this));
			});
			observer.observe(
				document.getElementById('reProfileWindow'), {
					attributes: true
				}
			);
		})();
		
		// プロフィール画像変更監視
		(()=>{
			const observer = new MutationObserver((mutations)=>{
				mutations.forEach(this.eventChangeProfimg.bind(this));
			});
			observer.observe(
				document.getElementById('reFace'), {
					attributes: true
				}
			);
		})();
		
		// 画像を発言欄にドロップするだけで、アップロードできるようにする
		document.querySelector('#sayField').addEventListener('drop', (e)=>{
			e.stopPropagation();
			e.preventDefault();
			console.log('file drop');
			
			// アップロードフォームを開く。
			document.querySelector('#iFace').click();
			const files = e.dataTransfer.files;
			if (files.length > 1){ return; alert('アップロードできるファイルは1つだけです。'); }
			
			// 複数回ファイルアップロードした場合、フォームが複数あるので一番下のフォームを操作する
			const fileUploadFormHolderS = document.querySelectorAll('.fileUploadFormHolder');
			const fileUploadFormHolder = fileUploadFormHolderS[fileUploadFormHolderS.length - 1];
			setTimeout(()=>{
				fileUploadFormHolder.querySelector('input').files = files;
			}, 1000);	// 画像読み込みの時間を待つ。500msだと失敗する場合がある
			setTimeout(()=>{
				fileUploadFormHolder.querySelectorAll('input')[1].click(); // アップロード実行
			}, 2000);
			console.log('file uploaded');
		});
	}
	
	// =========================================================================
	// 新規発言イベント
	// =========================================================================
	eventNewline(mutation){
		console.log('[新規発言イベントが発生');
		const newNode = mutation.addedNodes[0];
		if(!newNode){ return 0; }
		//console.log('[newNode', newNode);
		const msgs = document.querySelectorAll('#chatLog .message');
		const lastMsg = msgs[msgs.length - 1];
		const lastMsgText = lastMsg.innerText;
		console.log('[lastMsgText', lastMsgText);
		
		// 入力中タイマークリア
		if(this.typingTimeID){
			clearInterval(this.typingTimeID);
		}
		
		// クリックした発言をコンソールに表示
		if(1){
			const es = document.querySelectorAll('.message');
			for(let i=0; i<es.length; i++){
				es[i].onclick = function(e){
					console.log(e.target.innerText);
				}
			}
		}
		
		// 低コントラストモード　※動作しない
		if(0){
			//document.querySelector('#header img').src = '';
			
			const es = document.querySelectorAll('.message');
			for(let i=0; i<es.length; i++){
				const message = es[i];
				message.children[2].remove();	// 先頭から3つ削除
				message.children[1].remove();
				message.children[0].remove();
			}
		}
		
		// ====================================================================
		// システムメッセージ
		// ====================================================================
		if(newNode.classList.contains('systemMessage')){
			console.log('------------------------------------------');
			console.log('システムメッセージ。チャット開始か終了');
			console.log(newNode);
			
			// =================================================================
			// チャット終了
			
			// 相手がチャット終了
			if(lastMsgText.indexOf('チャット相手がチャットを終了したよ！') !== -1){
				console.log('%c[相手がチャット終了 systemMessage]', 'color:blue');
				document.querySelector('#sayFieldHolder2').style.border = 'solid 3px blue';
				this.gotoNextChat(5);
			}
			// 自分でチャット終了
			else if(lastMsgText.indexOf('\nチャットを終了したよ！') !== -1){
				console.log('%c[自分でチャット終了 systemMessage]', 'color:red');
				document.querySelector('#sayFieldHolder2').style.border = 'solid 3px red';
			}
			// マッチング中止
			else if(lastMsgText.indexOf('チャット相手を見つけるのをやめたよ！') !== -1){
				console.log('%c[マッチング中止 systemMessage]', 'color:green');
				document.querySelector('#sayFieldHolder2').style.border = 'solid 3px green';
			}
			
			// =================================================================
			// チャット開始
			
			else if(lastMsgText.indexOf('チャットを始めるよー！') !== -1){
				console.log('[チャットが始まったよー！]');
				
				// チャット開始からの秒数を表示する
				console.log('[チャット開始');
				const eChattime = document.getElementById('chattime');
				eChattime.innerText = '0';
				//setInterval(this.timerChatStart.bind(this), 1000);
				this.timerChatStartID = setInterval(this.timerChatStart.bind(this), 1000);
				
			}
			else{
				console.log('%c[チャット終了時のエラー？ systemMessage]', 'color:magenta');
			}
		}
		
		// ====================================================================
		// メッセージ入力中
		// ====================================================================
		else if(newNode.classList.contains('typing')){
			console.log('[メッセージ入力中');
			this.chatstartFlag = true;
			
			// 入力中の時間を計測開始
			this.typingTimeID = setInterval(this.timerTyping.bind(this), 100);
		}
		// ====================================================================
		// マイメッセージ
		// ====================================================================
		else if(newNode.classList.contains('myMessage')){
			console.log('[マイメッセージ');
			console.log(this.talks);
			this.chatstartFlag = true;
			
			// 発言履歴に追加
			const dt = new CDate2();
			const time = dt.getTime(':');
			console.log('talk', {'time': time, 'person': 'my', 'text': lastMsgText});
			this.talks.push({'time': time, 'person': 'my', 'text': lastMsgText});
		}
		// ====================================================================
		// 相手のメッセージ
		// ====================================================================
		else{
			console.log('[相手のメッセージ]');
			
			this.chatstartFlag = true;	// チャット相手が見つかった後に発言があった
			this.talkCount++;
			console.log('talkCount', this.talkCount);
			console.log(this.talks);
			
			// 発言履歴に追加
			const dt = new CDate2();
			const time = dt.getTime(':');
			console.log('talk', {'time': time, 'person': 'you', 'text': lastMsgText});
			this.talks.push({'time': time, 'person': 'you', 'text': lastMsgText});
			
			if(this.ngcheckEnabeld){
				this.yourMsg(lastMsgText);
			}
			
		}
	}
	// チャット開始からの秒数カウント
	timerChatStart(){
		const eChattime = document.getElementById('chattime');
		eChattime.innerText = (eChattime.innerText * 1) + 1;
		const elaspedTime = eChattime.innerText * 1;
		
		// 設定された待機時間を超えたら、次の相手を探す
		const nextChatWait = document.getElementById('inpNextChatWait').value * 1;
		const min = (nextChatWait - 3 > 0)? nextChatWait - 3: 0;
		const max = nextChatWait + 2;
		const rand = this.fysRandom(min, max);
		//console.log('rand', rand);
		if(!this.chatstartFlag & rand < elaspedTime){
			this.chatstartFlag = true;
			this.gotoNextChat(0);
			//location.reload();
		}
		
	}
	// 「入力中」になっている時間をカウントする
	timerTyping(){
		this.typingTime += 100;
		
		// 1秒毎の処理
		if(this.typingTime % 1000 === 0){
			console.log('typingTime', this.typingTime);
			
			// 10秒以上経過した場合
			if(this.typingTime > 10000 & this.talkCount === 0){
				console.log('相手は入力に10秒以上かかっています');
				if(1){
					console.log('次のマッチングへ行きます');
					this.gotoNextChat(1);
				}
			}
		}
	}
	
	// =========================================================================
	// 相手のメッセージ
	// =========================================================================
	yourMsg(lastMsgText){
		clearInterval(this.typingTimeID);
		console.log('入力中の時間は', this.typingTime, 'ms');
		
		if(this.talkCount === 1){ console.log('初めての相手の発言'); }
		
		// =====================================================================
		// 同時使用は相性が悪い処理
		
		// 相手の発言があったら全て切断　自動挨拶と同時に使うとおかしい
		if(0){
			this.gotoNextChat(this.fysRandom(2, 3));
		}
		else{
			// 自動挨拶　全切断と同時に使うとおかしい
			if(0){
				if(this.talkCount === 1 & lastMsgText === 'こんばんは'){
					doCMD({
						cmd: "d"
					});
					this.say('こんばんは', 3.5);
					
					// 機能追加。自動挨拶後、返事がなかったら切断
				}
				
			}
			else{
				// コピペとボットの自動切断
				if(1){
					if(this.typingTime === 0){
						console.log('この発言はコピペかボットなので次へ行きます');
						this.gotoNextChat(2);	// 切断時間と自動挨拶の時間注意
					}
				}
				
			}
		}
		
		// ---------------------------------------------------------------------
		
		if(0){
			if(this.talkCount === 1 & this.indexOfList(lastMsgText, [
				'ｗｗｗ', 'www'
			]) !== -1){
				this.say('大草原');
				this.gotoNextChat(2);
			}
		}
		
		// =====================================================================
		// プロフィールと組み合わせた判断
		
		if(1){
			// 相手のプロフィール
			const yourProf = this.getYourProf();
			//console.log('['+ yourProf.text.trim() +']');
			//console.log(yourProf.imgurl);
			//console.log(yourProf.url);
			
			if(this.talkCount < 3 & this.indexOfList(lastMsgText, [
				'こんばん', 'こんにち',
				'こん', 
				'おは',
			]) !== -1){
				if(yourProf.text.trim() === '男'
				| this.indexOfList(yourProf.text, [
					'おとこ', '男性', '男子', '男',
					'男子',
					'アパレル',
					'既婚',
					'エッチ', 'おっぱい',
				]) !== -1){
					//this.say('こんばんは！');
					this.gotoNextChat(2);
				}
			}
		}
		
		// =====================================================================
		// 一般的なNGワードで切断
		
		// 部分一致のNGワード
		if(this.talkCount < 4 & this.indexOfList(lastMsgText, [
			'♂', '男', 'おとこ', 'オトコ',
			'女性ですか',
			'何歳ですか',
			'低能',
			'挨拶', '自殺', '死', 'バカ', 'ゴミ', 'クズ', 
			'おい', 'こら', 'おまえ', 'お前', 
			'年収',
			'セックス', '童貞',
		]) !== -1){
			this.gotoNextChat(2);
		}
		
		// 完全一致のNGワード
		for(const blacklist of [
			//'こんばんは',
			'こんにちは男', 'こんにちは♂', 
			'こんばんは男', 'こんばんは♂',
			'男', 'おとこ', '男です', '男性です', '男性です。', 'おとこです',
			'女性？', '女子？', '女性ですか', '女性ですか？',
			'jk?', 'jk？',
			'見せて',
			'うざい', '死ね', 'キモ',
			'ちんこ', 'うんこ', 'うんち',
			'まじかｗ', 'うい',
			//'何話す？', '何話しますか？',
			'',
		]){
			if(this.talkCount < 4 & lastMsgText === blacklist){
				this.gotoNextChat(1);
			}
		}
		
		// 長文切断
		if(0){
			if(lastMsgText.length > 40){ this.gotoNextChat(2); }
		}
		
		// =====================================================================
		// お遊び機能
		
		// サイコロ
		if(1){
			if(lastMsgText === 'サイコロ'){
				const r = this.fysRandom(1, 6);
				this.say('がらんがらんがらん');
				this.say('サイコロは「' + r + '」です', 3);
			}
		}
		
	}
	
	// =========================================================================
	// プロフィールが出現したイベント
	// =========================================================================
	eventEmergeProf(mutation){
		console.log('[yourProf]');
		const yourProf = this.getYourProf();
		console.log('['+ yourProf.text.trim() +']');
		console.log(yourProf.imgurl);
		console.log(yourProf.url);
		
		// =====================================================================
		// プロフィール画像による判断
		// =====================================================================
		
		// プロフィール画像あり
		if(yourProf.imgurl !== 'http://chatpad.jp/chatroom.html#'){
			console.log('プロフィール画像あり');
			
			// プロフィール画像がある人を全て切断
			if(0){
				this.gotoNextChat(3);
			}
			if(1){
				// 特定のプロフィール画像の人を自動切断　アップロード毎に画像URL変化する
				if(this.indexOfList(yourProf.imgurl, [
					'1644488361119'
				]) !== -1){
					this.gotoNextChat(2);
				}
			}
			
		}
		// プロフィール画像なし
		else{
			console.log('プロフィール画像なし');
		}
		
		// =====================================================================
		// プロフィールテキストによる判断
		// =====================================================================
		
		// プロフィールのブラックリスト　部分一致
		if(this.indexOfList(yourProf.text, [
			//'♂', '男性', 'おとこ',
			'熟女好き', 'セフレ', '男32', '障害者32',
			'既婚', '女性希望',
			'エッチ','うんこ', '射精',
			'おじさんです',
			//'オジサンさ',
			'富裕層',
			'妻は大きい', '彼女は大きい', '年収100万円',
			'アパレル', '消防士', 'マッチョ',
			
		]) !== -1){
			console.log('[プロフィールにNGワードを含むので次へ行きます');
			this.gotoNextChat(2);
		}
		
		// 
		if(1){
			if(this.indexOfList(yourProf.text, ['ＪＤ２１', 'JD21']) !== -1){
				this.gotoNextChatR(3, 1, 1);
			}
		}
	}
	
	// =========================================================================
	// プロフィールテキスト変更イベント
	// =========================================================================
	eventChangeProftext(mutation){
		console.log('change prof text');
	}
	
	// =========================================================================
	// プロフィール画像変更イベント
	// =========================================================================
	eventChangeProfimg(mutation){
		console.log('change prof img');
	}
	
	// =========================================================================
	// プロフィール自動設定
	// =========================================================================
	setProf(){
		// ランダムプロフィール
		if(0){
			let pfText = [
				'prof1',
				'prof2',
				'prof3',
			];
			const r = this.fysRandom(1, pfText.length - 1);
			doSyncCMD({
				cmd: 'p',
				content: encodeCMDStr(pfText[r])
			});
			console.log(r, pfText[r]);
		}
	}
	
	// =========================================================================
	// UI
	// =========================================================================
	buildUI(){
		document.body.oncopy = null;
		
		const header = document.querySelector('#header');
		
		// rootPanel
		const rootPanel = (()=>{
			const div = document.createElement('div');
			div.id = 'rootPanel';
			div.style.marginLeft = '250px';
			div.style.border = 'solid #ccc 1px';
			div.style.width = '500px';
			div.style.height = '35px';
			
			return div;
		})();
		header.appendChild(rootPanel);
		
		// btnSaveChatlog
		const btnSaveChatlog = (()=>{
			const btn = document.createElement('button');
			btn.id = 'btnSaveChatlog';
			btn.style.float = 'left';
			btn.style.color = 'black';
			btn.style.background = '#ccc';
			btn.style.border = 'solid gray 1px';
			btn.appendChild(document.createTextNode('saveChatlog'));
			
			btn.addEventListener('click', (e)=>{
				console.log('saveChatlog');
				this.saveChatlog3();
			});
			
			return btn;
		})();
		rootPanel.appendChild(btnSaveChatlog);
		
		// btnSaveYourimg
		const btnSaveYourimg = (()=>{
			const btn = document.createElement('button');
			btn.id = 'btnSaveYourimg';
			btn.style.float = 'left';
			btn.style.marginLeft = '3px';
			btn.style.color = 'black';
			btn.style.background = '#ccc';
			btn.style.border = 'solid gray 1px';
			btn.appendChild(document.createTextNode('saveYourimg'));
			
			btn.addEventListener('click', (e)=>{
				console.log('saveYourimg 開発中');
				
				// 今の方法ではサムネイルしか保存できない
				// ファイル名は元のファイル名を使えたら使う？
				const dt = new CDate2();
				const date = dt.getDate('-');	// yyyy-mm-dd
				const time = dt.getTime('-');	// hh-mm-dd
				const filename = `chatpad_${date}_${time}.jpg`;	// chatpad_yyyy-mm-dd_hh-mm-dd.txt
				const url = document.querySelector('#reFace').src;
				//const url = document.querySelector('#reFaceFrame a').href;	// チャットパッド
				this.downloadImg(filename, url);
			});
			
			return btn;
		})();
		rootPanel.appendChild(btnSaveYourimg);
		
		// チャット相手が見つかってからの経過時間表示欄
		const divChattime = (()=>{
			const div = document.createElement('div');
			div.id = 'chattime';
			div.style.float = 'left';
			div.style.width = '30px';
			div.style.paddingTop = '2px';
			div.style.textAlign = 'right';
			div.appendChild(document.createTextNode('time'));
			
			return div;
		})();
		rootPanel.appendChild(divChattime);
		
		// チャット相手が見つかってからの待機時間設定欄
		const inpNextChatWait = (()=>{
			const inp = document.createElement('input');
			inp.id = 'inpNextChatWait';
			inp.type = 'number';
			inp.step = 2;
			inp.min = 2;
			inp.style.float = 'left';
			inp.style.width = '40px';
			
			const nextChatWait = localStorage.getItem('nextChatWait') * 1;
			console.log(nextChatWait);
			inp.value = (nextChatWait)? nextChatWait: 10;
			
			inp.addEventListener('change', (e)=>{
				const nextChatWait = e.target.value;
				localStorage.setItem('nextChatWait', nextChatWait);
			});
			
			return inp;
		})();
		rootPanel.appendChild(inpNextChatWait);
		
		// 自動巡回モード切り替えチェックボックス
		const lblAutoNextChat = (()=>{
			const lbl = document.createElement('label');
			lbl.appendChild(document.createTextNode('autoNext'));
			lbl.style.float = 'left';
			lbl.style.paddingTop = '2px';
			
			return lbl;
		})();
		const chkAutoNextChat = (()=>{
			const inp = document.createElement('input');
			inp.id = 'inpAutoNextChat';
			inp.type = 'checkbox';
			inp.checked = 'checked';
			inp.style.float = 'left';
			
			inp.addEventListener('click', (e)=>{
				console.log(e.target.checked);
				if(!e.target.checked){
					clearInterval(this.timerChatStartID);
					clearInterval(this.typingTimeID);
				}
				this.chatstartFlag = !this.chatstartFlag;
			});
			
			return inp;
		})();
		lblAutoNextChat.appendChild(chkAutoNextChat);
		rootPanel.appendChild(lblAutoNextChat);
		
		// NGcheck
		const lblNgcheck = (()=>{
			const lbl = document.createElement('label');
			lbl.appendChild(document.createTextNode('NG check'));
			lbl.style.float = 'left';
			lbl.style.paddingTop = '2px';
			
			return lbl;
		})();
		const chkNgcheck = (()=>{
			const inp = document.createElement('input');
			inp.id = 'inpNgcheck';
			inp.type = 'checkbox';
			inp.checked = 'checked';
			inp.style.float = 'left';
			
			inp.addEventListener('click', (e)=>{
				this.ngcheckEnabeld = !this.ngcheckEnabeld;
			});
			
			return inp;
		})();
		lblNgcheck.appendChild(chkNgcheck);
		rootPanel.appendChild(lblNgcheck);
		
		// 挨拶ボタン
		const btnHello = (()=>{
			const btn = document.createElement('button');
			btn.id = 'btnHello';
			btn.style.float = 'left';
			btn.style.color = 'black';
			btn.style.background = '#ccc';
			btn.style.border = 'solid gray 1px';
			btn.appendChild(document.createTextNode('hello'));
			
			btn.addEventListener('click', (e)=>{
				console.log('hello');
				this.say('こんばんは', 2);
			});
			
			return btn;
		})();
		rootPanel.appendChild(btnHello);
		
		// btnNextChat
		const btnNextChat = (()=>{
			const btn = document.createElement('button');
			btn.id = 'btnNextChat';
			btn.setAttribute('accesskey', 'x');
			btn.style.color = 'black';
			btn.style.background = '#ccc';
			btn.style.border = 'solid gray 1px';
			btn.appendChild(document.createTextNode('NextChat'));
			btn.style.position = 'absolute';	// disconnectButtonに重ねる
			btn.style.top = '0px';
			btn.style.left = '0px';
			
			btn.addEventListener('click', (e)=>{
				console.log('next');
				document.getElementById('disconnectButton').click();
				setTimeout(()=>{	// チャット終了と新規チャットは同じボタン
					document.getElementById('disconnectButton').click();
				}, 100);
			});
			
			return btn;
		})();
		const disconnectButtonParent = document.getElementById('disconnectButton').parentElement;
		disconnectButtonParent.style.position = 'relative';
		disconnectButtonParent.appendChild(btnNextChat);
	
	}
	
	// チャットログ取得
	saveChatlog3(){
		// 時刻付きログ
		const log2 = (()=>{
			const yourProf = this.getYourProf();
			console.log(yourProf);
			
			// ログ整形
			let log = yourProf.imgurl + '\n\n' + yourProf.text + '\n'.repeat(8);
			for(const talk of this.talks){
				const ltime = talk.time;	// 発言時刻は自分のPCに表示された時刻
				const person = talk.person;
				const text = talk.text;
				log += `${ltime}\t${person}\t${text}\n`;
			}
			
			return log;
		})();
		
		// 簡易型ログ
		const log1 = (()=>{
			// HTMLをパースしてログ取得
			const messages = document.querySelectorAll('.message');
			let log = '';
			for(var i=3; i<messages.length; i++){
				const msg = messages[i];
				console.log(msg);
				let type = null;
				if(msg.classList.contains('systemMessage')){
					continue;	// 何もせず次の周回
				}
				else if(msg.classList.contains('myMessage')){
					type = 'my';
				}
				else{
					type = 'you';
				}
				log += type +'\t'+ msg.innerText +'\n';
			}
			//console.log(log);
			
			return log;
		})();
		
		// ファイル名
		const dt = new CDate2();
		const date = dt.getDate('-');	// yyyy-mm-dd
		const dttime = dt.getTime('-');	// hh-mm-dd
		const filename = `chatpad_${date}_${dttime}.txt`;	// chatpad_yyyy-mm-dd_hh-mm-dd.txt
		
		// ダウンロード実行
		const log = log2 +'\n'.repeat(10) + log1;
		this.download(filename, log);
	}
	// 発言　wait:待ち時間の秒数
	say(text, wait){
		setTimeout(()=>{
			document.getElementById('sayField').value = text;
			setTimeout(()=>{
				document.getElementById('sayButton').click();
			}, 100);
		}, wait * 1000);
	}
	// 次のマッチングに進む。固定時間版
	// wait：	実行までの待ち時間の秒数
	gotoNextChat(wait){
		setTimeout(()=>{
			//document.querySelector('#chattime').style.border = 'solid 1px red';
			//document.querySelector('#sayFieldHolder2').style.border = 'solid 3px red';
			document.getElementById('btnNextChat').click();
		}, wait * 1000);
	}
	// 次のマッチングに進む。乱数版
	// nextChatWait：	実行までの待ち時間の秒数
	// min：	最低待ち時間。nextChatWaitから引く秒数
	// max：	最大待ち時間。nextChatWaitに足す秒数
	// 例：	「gotoNextChatR(5, 1, 2)」の場合、4秒～7秒の待ち時間。
	gotoNextChatR(nextChatWait, min, max){
		const rand = this.fysRandom(nextChatWait - min, nextChatWait + max);
		//console.log('rand', rand);
		setTimeout(()=>{
			document.getElementById('btnNextChat').click();
		}, rand);
	}
	
	// =========================================================================
	// ユーティリティ関数
	// =========================================================================
	 
	// テキストファイルダウンロード
	download(filename, data){
		const blob = new Blob([data], {'type': 'text/plain'});
		const url = URL.createObjectURL(blob);
		const a = document.createElement('a');
		document.body.appendChild(a);
		a.download = filename;
		a.href = url;
		a.click();
		a.remove();
		URL.revokeObjectURL(url);
	}
	// 画像ファイルダウンロード
	downloadImg(filename, url){
		const img = document.createElement('img');
		img.src = url;
		document.body.appendChild(img);
		
		setTimeout(()=>{
			const a = document.createElement('a');
			a.href= url;
			a.download = filename;
			document.body.appendChild(a);
			a.click();
			a.remove();
		}, 1000);
	}
	// プロフィール取得
	getYourProf(){
		const text = document.getElementById('reProfileMargin').innerText;
		const url = document.getElementById('reURLMargin').innerText;
		//var imgurl = document.getElementById('reFace').src;
		const imgurl = document.querySelector('#reFaceFrame a').href;
		
		//console.log(text);
		//console.log(url);
		//console.log(imgurl);
		
		return { 'text': text, 'url': url, 'imgurl': imgurl }
	}
	// 複数の文字列を検索して最初に見つかった文字列の要素番号を返す。見つからない場合は-1
	indexOfList(targetString, list){
		let count = 0;
		for(const search of list){
			if(targetString.indexOf(search) !== -1){
				//console.log(search);
				return count;
			}
			//console.log(search);
			count++;
		}
		return -1;
	}
	// 乱数
	fysRandom(min, max){
		let arr2 = [];
		for(let i=min; i<max+1; i++){
			arr2.push(i);
		}
		let a = arr2.length;
		
		//シャッフルアルゴリズム
		while(a){
			const j = Math.floor( Math.random() * a );
			const t = arr2[--a];
			arr2[a] = arr2[j];
			arr2[j] = t;
		}
		
		return arr2[0];
	}
	random(min, max) {
		const random = Math.floor( Math.random() * (max + 1 - min) ) + min;
		
		return random;
	}
}

class CDate2{
	constructor(){
		this.t = new Date();
	}
	getDate(separator){
		var t = this.t;
		var y = t.getFullYear();
		var m = ('00' + (t.getMonth() + 1)).slice(-2)
		var d = ('00' + t.getDate()).slice(-2);
		
		var dateString = 'error';
		if(separator){
			// 指定されたセパレータで区切る
			dateString = [y, m, d].join(separator);
		}
		else{
			dateString = y+m+d;	// yyyymmdd
		}
		
		return dateString;
	}
	getTime(separator){
		//var t = new Date();
		var t = this.t;
		//var h = ('00' + t.getHours() ).slice(-2);
		var h = ('00' + this.getHoursJtc()).slice(-2);	// タイムゾーン設定が狂っててもJTCとして取得する
		var m = ('00' + t.getMinutes()).slice(-2);;
		var s = ('00' + t.getSeconds()).slice(-2);
		
		var timeString = 'error';
		if(separator){
			// 指定されたセパレータで区切る
			timeString = [h, m, s].join(separator);
		}
		else{
			timeString = h+m+s;	// hhmmss
		}
		
		return timeString;
	}
	getHoursJtc(){
		var t = this.t;
		var h = ('00' + t.getHours() ).slice(-2);
		h *= 1;	// 数値に変換
		
		// UTCだったら、JTCに変換する
		if(t.getUTCHours() === t.getHours()){
			h += 9;
			if(12 > h){
				h -= 16;
			}
		}
		
		return h;
	}
	isUtc(){
		var t = this.t;
		
		return t.getUTCHours() === t.getHours();
	}
}

const cp = new ChatPadu();
